﻿using System;
using ArrayHelper;
using RectangleHelper;

namespace MyProgramm
{
    class Program
    {
        static string StringTransformation(string str) // функция удаляет лишние пробелы в строке, меняет . на , в числах
        {
            while (str.StartsWith(" "))
            {
                str = str.Remove(0, 1);
            }
            while (str.EndsWith(" "))
            {
                str = str.Remove(str.Length - 1, 1);
            }
            while (str.Contains("  "))
            {
                str = str.Replace("  ", " ");
            }
            while (str.Contains("."))
            {
                str = str.Replace(".", ",");
            }
            return str;
        }

        static string ArrayTransforming(string str, bool asc)
        {
            str = StringTransformation(str);
            string[] dataStr = str.Split(' ');
            string res;
            if (str.Contains(","))
            {
                try
                {
                    double[] data = Array.ConvertAll(dataStr, Double.Parse);
                    if (asc)
                    {
                        MyArrayHelper.AscArrange(data);
                    }
                    else
                    {
                        MyArrayHelper.DescArrange(data);
                    }
                    res = String.Join(" ", data);
                }
                catch
                {
                    res = ("Введены недопустимые символы");
                }
            }
            else
            {
                if (str.Length > 0)
                {
                    try
                    {
                        int[] data = Array.ConvertAll(dataStr, Int32.Parse);
                        if (asc)
                        {
                            MyArrayHelper.AscArrange(data);
                        }
                        else
                        {
                            MyArrayHelper.DescArrange(data);
                        }
                        res = String.Join(" ", data);
                    }
                    catch
                    {
                        res = "Введены недопустимые символы";
                    }
                }
                else
                {
                    res = "Массив пустой";
                }
            }
            return res;
        }
        static void Main(string[] args)
        {
            bool notFinish = true;
            while (notFinish)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("1. Сортировка массива по возрастанию");
                Console.WriteLine("2. Сортировка массива по убыванию");
                Console.WriteLine("3. Вычисление суммы всех положительных членов двумерного массива");
                Console.WriteLine("4. Вычисление периметра прямоугольника");
                Console.WriteLine("5. Вычисление площади прямоугольника");
                Console.WriteLine("6 (и любые другие символы) - Выход");
                Console.WriteLine();
                Console.Write("Выберите требуемое действие: ");
                string strMenu = Console.ReadLine();
                Console.WriteLine();
                string result, str, str1;
                switch (strMenu)
                {
                    case "1":
                        Console.WriteLine("Введите массив чисел через пробел:");
                        str = Console.ReadLine();
                        result = ArrayTransforming(str, true);
                        break;
                    case "2":
                        Console.WriteLine("Введите массив чисел через пробел:");
                        str = Console.ReadLine();
                        result = ArrayTransforming(str, false);
                        break;
                    case "3":
                        try
                        {
                            Console.Write("Введите количество строк в массиве: ");
                            int n1 = Int32.Parse(Console.ReadLine());
                            double[][] array2 = new double[n1][];
                            for (int i = 1; i <= n1; i++)
                            {
                                Console.Write("Введите элементы строки ");
                                Console.Write(i);
                                Console.Write(" массива: ");
                                str = Console.ReadLine();
                                str = StringTransformation(str);
                                string[] dataStr = str.Split(' ');
                                double[] str2 = Array.ConvertAll(dataStr, Double.Parse);
                                array2[i - 1] = str2;
                            }
                            result = Convert.ToString(MyArrayHelper.PositiveElementSum(array2));
                        }
                        catch
                        {
                            result = ("Ошибка ввода");
                        }
                        break;
                    case "4":
                        Console.Write("Введите длину прямоугольника: ");
                        str = Console.ReadLine();
                        str = StringTransformation(str);
                        Console.Write("Введите высоту прямоугольника: ");
                        str1 = Console.ReadLine();
                        str1 = StringTransformation(str1);
                        try
                        {
                            result = MyRectangleHelper.Perimetr(str, str1);
                        }
                        catch
                        {
                            result = "Введены недопустимые символы";
                        }
                        break;
                    case "5":
                        Console.Write("Введите длину прямоугольника: ");
                        str = Console.ReadLine();
                        str = StringTransformation(str);
                        Console.Write("Введите высоту прямоугольника: ");
                        str1 = Console.ReadLine();
                        str1 = StringTransformation(str1);
                        try
                        {
                            result = MyRectangleHelper.Square(str, str1);
                        }
                        catch
                        {
                            result = "Введены недопустимые символы";
                        }
                        break;
                    default:
                        result = "Выход";
                        notFinish = false;
                        break;
                }
                Console.WriteLine();
                Console.WriteLine("Реультат:");
                Console.WriteLine(result);
                Console.WriteLine();
                Console.WriteLine();
            }
        }
            
    }
}
