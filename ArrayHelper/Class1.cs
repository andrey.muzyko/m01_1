﻿using System;

namespace ArrayHelper
{
    public class MyArrayHelper
    {
        public static int[] AscArrange(int[] dataArray)
        {
            for (int i=0; i < dataArray.Length-1; i++)
            {
                for (int j = 0; j < dataArray.Length-1-i; j++)
                {
                    if (dataArray[j] > dataArray[j+1])
                    {
                        int buf = dataArray[j];
                        dataArray[j] = dataArray[j + 1];
                        dataArray[j + 1] = buf;
                    }
                }
            }
            return dataArray;
        }

        public static double[] AscArrange(double[] dataArray)
        {
            for (int i = 0; i < dataArray.Length - 1; i++)
            {
                for (int j = 0; j < dataArray.Length - 1 - i; j++)
                {
                    if (dataArray[j] > dataArray[j + 1])
                    {
                        double buf = dataArray[j];
                        dataArray[j] = dataArray[j + 1];
                        dataArray[j + 1] = buf;
                    }
                }
            }
            return dataArray;
        }
        public static int[] DescArrange(int[] dataArray)
        {
            for (int i = 0; i < dataArray.Length - 1; i++)
            {
                for (int j = 0; j < dataArray.Length - 1 - i; j++)
                {
                    if (dataArray[j] < dataArray[j + 1])
                    {
                        int buf = dataArray[j];
                        dataArray[j] = dataArray[j + 1];
                        dataArray[j + 1] = buf;
                    }
                }
            }
            return dataArray;
        }

        public static double[] DescArrange(double[] dataArray)
        {
            for (int i = 0; i < dataArray.Length - 1; i++)
            {
                for (int j = 0; j < dataArray.Length - 1 - i; j++)
                {
                    if (dataArray[j] < dataArray[j + 1])
                    {
                        double buf = dataArray[j];
                        dataArray[j] = dataArray[j + 1];
                        dataArray[j + 1] = buf;
                    }
                }
            }
            return dataArray;
        }
        public static double PositiveElementSum(double[][] dataArray)
        {
            double sum = 0;
            for (int i = 0; i < dataArray.Length; i++)
            {
                for(int j = 0; j < dataArray[i].Length; j++)
                {
                    if (dataArray[i][j] > 0)
                    {
                        sum += dataArray[i][j];
                    }
                }
            }
            return sum;
        }
    }
}
