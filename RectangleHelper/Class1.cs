﻿using System;

namespace RectangleHelper
{
    public class MyRectangleHelper
    {
        public static string Perimetr(string width, string height)
        {
            double widthD = Double.Parse(width);
            double heightD = Double.Parse(height);
            string res = Convert.ToString((widthD + heightD) * 2);
            return res;
        }


        public static string Square(string width, string height)
        {
            double widthD = Double.Parse(width);
            double heightD = Double.Parse(height);
            string res = Convert.ToString(widthD * heightD);
            return res;
        }
        
    }
}
